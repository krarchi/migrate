from flask import *
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import base64
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root:1234@localhost/users'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app,db)

class content(db.Model):
    email = db.Column(db.String(20), primary_key = True)
    password = db.Column(db.String(20),nullable = False)

    def __init__(self,username,password):
        self.email = username
        self.password = password

@app.route('/')
def home():
    return render_template("Login.html")

@app.route('/rr')
def rr():
    return render_template("Registration.html")

@app.route('/welcome')
def welcome():
    return "Welcome User- You have logged in successfully"

@app.route('/login',methods=['POST'])
def login():
    try:
        username = request.form['username']
        password = request.form['password']
        
        user = content.query.filter_by(email = username).first()
        pas =  base64.b64decode(user.password)
        if password == pas:
            return redirect(url_for('welcome'))
        else:
            print "Incorrect password"
            return redirect(url_for('failed'))
    except Exception as e:
        print e
        return redirect(url_for('failed'))
@app.route('/register',methods=['POST'])
def register():
    try:
        username = request.form['email']
        password = request.form['psw']
        r_password = request.form['psw-repeat']
        if password == r_password:
            password = base64.b64encode(r_password)
            user = content(username,password)
            db.session.add(user)
            db.session.commit()
            return "You have registered Successfully"
        else:
            print "Password didn't match"
            return redirect(url_for('failed'))
    except Exception as e:
        print e
        return redirect(url_for('failed'))
@app.route('/failed')
def failed():
    return "Your attempt failed / goto:- http://127.0.0.1:5000/"  

if __name__ == '__main__':
   db.create_all()
   app.run(debug = True)      